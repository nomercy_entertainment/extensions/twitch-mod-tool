import plugins from '../plugins/index.js';

const backgroundElement = document.querySelector('#background-menu');
const layoutElement = document.querySelector('#layout-menu');
const chatElement = document.querySelector('#chat-menu');

let enabledCount = 0;
let api;

function isChrome() {
	return typeof chrome !== 'undefined' && typeof chrome.runtime !== 'undefined';
}

function isFirefox() {
	return typeof browser !== 'undefined' && typeof browser.runtime !== 'undefined';
}

if (isChrome()) api = chrome;
else if (isFirefox()) api = browser;

function changeIcon(iconName) {
	if (api.action !== undefined) api.action.setIcon({ path: `/icons/${iconName}` });
	else if (api.browserAction !== undefined) api.browserAction.setIcon({ path: `/icons/${iconName}` });
	else console.log('changing icon is not supported');
}

// eslint-disable-next-line no-unused-vars
chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
	if (msg.action === 'updateIcon') {
		changeIcon(msg.file);
	}
});

plugins.forEach((plugin) => {
	const plugin_id = `plugin_${plugin.id}`;
	chrome.storage.local.get(plugin_id, (savedSettings) => {
		savedSettings = savedSettings[plugin_id] || plugin.settings;
		const labelElement = document.createElement('label');
		labelElement.setAttribute('for', plugin_id);
		const inputElement = document.createElement('input');
		inputElement.setAttribute('type', 'checkbox');
		inputElement.setAttribute('id', plugin_id);
		inputElement.setAttribute('name', plugin_id);
		inputElement.setAttribute('role', 'switch');
		inputElement.checked = savedSettings.enabled;

		inputElement.addEventListener('input', () => {
			savedSettings.enabled = inputElement.checked;
			chrome.storage.local.set({
				[plugin_id]: savedSettings,
			});

			plugin.settings.enabled = inputElement.checked;
			if (inputElement.checked) {
				enabledCount += 1;
			} else {
				enabledCount -= 1;
			}
			changeIcon(enabledCount > 0 ? 'mod-48x48.png' : 'twitch-48x48.png');
		});

		if (savedSettings.enabled) {
			enabledCount += 1;
		}

		const spanElement = document.createElement('span');
		spanElement.textContent = plugin.name;
		labelElement.append(inputElement);
		labelElement.append(spanElement);

		if (plugin.type === 'chat') {
			chatElement.append(labelElement);
		} else if (plugin.type === 'layout') {
			layoutElement.append(labelElement);
		} else if (plugin.type === 'backgound') {
			backgroundElement.append(labelElement);
		}
	});

	setTimeout(() => {
		changeIcon(enabledCount > 0 ? 'mod-48x48.png' : 'twitch-48x48.png');
	}, 500);
});

if (plugins.filter((p) => p.type === 'background').length > 0) {
	document.querySelector('#background').style.display = 'block';
}
if (plugins.filter((p) => p.type === 'chat').length > 0) {
	document.querySelector('#chat').style.display = 'block';
}
if (plugins.filter((p) => p.type === 'layout').length > 0) {
	document.querySelector('#layout').style.display = 'block';
}
