const queryParserObjArr = () => {
	if (location.search == '') return {};
	const result = {};
	location.search
		.replace('?', '')
		.split('&')
		.map((s) => {
			const [k, v] = s.split('=');
			result[k] = v;
		});
	return result;
};

const queryParserKeyVal = () => {
	if (location.search == '') return [];
	return location.search
		.replace('?', '')
		.split('&')
		.map((s) => {
			const [k, v] = s.split('=');
			return {
				key: k,
				value: v,
			};
		});
};

const dispatchEvent = (name, data) => {
	document.body.dispatchEvent(new CustomEvent(name, { detail: data }));
};

let pathname = location.pathname;
function routeChangeEvent() {
	if (pathname !== location.pathname) {
		dispatchEvent('routeChange', {
			previous: pathname,
			current: location.pathname,
			isRaid: queryParserObjArr()['referrer'] == 'raid',
		});
		pathname = location.pathname;
	}
}

const eventObserver = () => {
	const targetNode = document.querySelector('.twilight-main');
	if (targetNode) {
		const observer = new MutationObserver((mutationsList) => {
			routeChangeEvent();		
		});

		const config = { attributes: true, childList: true, subtree: true };
		observer.observe(targetNode, config);
	}
};

const startBackground = async () => {
	
	chrome.runtime.getURL('plugins/index.js');
	
	eventObserver();
};

startBackground();
