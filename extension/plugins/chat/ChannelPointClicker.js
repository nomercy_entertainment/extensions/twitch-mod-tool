import Plugin from "../../lib/Plugin.js";

class ChannelPointClicker extends Plugin {
	constructor() {
		super('Auto click on channel point redemption');
	}

	id = 'b06142d9-06c7-42d4-bf38-ac59baff8da3';

	type = 'chat';

	settings = {
		enabled: false,
	};

	observer = null;

	callback(mutationsList, observer) {
		for(const mutation of mutationsList) {
			if (mutation.type === 'childList') {
				if(mutation.addedNodes.length > 0) {
					const node = mutation.addedNodes[0];
					if(node && typeof node != 'object') {
						const btn = node?.querySelector('button');					
						if (btn) {
							btn.click();
							console.log( new Date() + ' clikker clicked');
						}
					}
				}
			}
		}
	}

	clickBonus() {
		const targetNode = document.querySelector('.community-points-summary > div:last-child')?.querySelector('button');
		if (targetNode) {
			targetNode.click();
			console.log( new Date() + ' clikker clicked');
		}
	}

	init() {
		const targetNode = document.querySelector('.community-points-summary');
		if(targetNode){
			this.observer = new MutationObserver(this.callback);
	
			const config = { attributes: true, childList: true, subtree: true };
			this.observer.observe(targetNode, config);
	
			this.clickBonus();
		}
	}

	run() {
		this.init();
	}

	cleanup() {
		this.observer.disconnect();
	}

	always() {}
}

export default ChannelPointClicker;
