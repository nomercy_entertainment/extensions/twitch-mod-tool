import Plugin from "../../lib/Plugin.js";

class SwapChat extends Plugin {
	constructor() {
		super('Move chat to the left of screen');
	}

	id = 'c677a9f5-8fa7-42e9-8467-a222690200b7';

	type = 'layout';

	settings = {
		enabled: false,
	};

	init() {
		setTimeout(() => {
			this.flipFlex();
			this.showHideChat();
			setTimeout(() => {
				this.moveChannelInfo();
				this.movePlayer();
				this.moveChat();
				setTimeout(() => {
					this.moveSidenav();
					this.showHideNav();
				}, 50);
			}, 100);
		}, 30);
	}

	docClickRegister() {
		document.onclick = null;
		document.addEventListener('click', (e) => {
			if (!e.target.closest('a')) {
				return;
			}
			setTimeout(() => {
				this.init();
			}, 50);
		});
	}

	showHideNav(enabled) {
		setTimeout(() => {
			const sideNavHeaderTitle = document.querySelector('.side-nav-header');
			if (sideNavHeaderTitle && enabled) {
				sideNavHeaderTitle.style.display = 'flex';
				sideNavHeaderTitle.style.justifyContent = 'end';
			} else if (sideNavHeaderTitle) {
				sideNavHeaderTitle.style.textAlign = '';
				sideNavHeaderTitle.style.marginRight = '';
			}
		}, 50);

		const sideNavToggleIcon = document.querySelector('[data-test-selector="side-nav__visibility-toggle"]')?.querySelector('path');
		if (sideNavToggleIcon && this.settings.enabled) {
			sideNavToggleIcon.style.transform = 'rotate(180deg) translate(-100%, -100%)';
		} else if (sideNavToggleIcon) {
			sideNavToggleIcon.style.transform = '';
		}
	}

	showHideChat(enabled) {
		setTimeout(() => {
			const chatTransformContainer = document.querySelector('.channel-root__right-column');
			const chatToggleButton = document.querySelector('[data-a-target*="right-column__toggle-collapse-btn"]')?.parentElement.parentElement;
			const screenHasPlayer = document.querySelector('.channel-root--watch') != null;

			if (!screenHasPlayer && chatToggleButton) {
				chatToggleButton.style.left = '-3.5rem';
				return;
			}
			
			if (chatTransformContainer && enabled) {
				chatTransformContainer.style.transform = 'none';
				chatTransformContainer.style.transition = '';
			} else if (chatTransformContainer) {
				chatTransformContainer.style.transform = 'translateX(-34rem) translateZ(0px)';
				chatTransformContainer.style.transition = '';
				setTimeout(() => {
					chatTransformContainer.style.transition = 'transform 500ms ease 0ms';
				}, 500);
			}

			if (chatToggleButton && enabled) {
				if (document.querySelector('[data-a-target="right-column-chat-bar-collapsed"]')) {
					chatToggleButton.style.left = '.5rem';
				} else {
					chatToggleButton.style.left = '30rem';
				}
			} else if (chatToggleButton) {
				chatToggleButton.style.transition = 'none';
				chatToggleButton.style.transform = '';
				chatToggleButton.style.left = '';
			}
		}, 100);
	}

	flipFlex() {
		const sideNav = document.querySelector('#sideNav');
		const parent = sideNav.parentNode;
		if (parent && this.settings.enabled) {
			parent.style.flexDirection = 'row-reverse';
		} else if (parent) {
			parent.style.flexDirection = 'row';
		}
	}

	moveChannelInfo() {
		const channelInfoWrapper = document.querySelector('.channel-info-content');
		if (channelInfoWrapper && this.settings.enabled) {
			channelInfoWrapper.style.marginLeft = 'auto';
		} else if (channelInfoWrapper) {
			channelInfoWrapper.style.marginLeft = '';
		}
	}

	movePlayer() {
		const screenHasPlayer = document.querySelector('.channel-root--watch') != null;
		if (!screenHasPlayer) {
			return;
		}

		const videoplayerWrapper = document.querySelector('.persistent-player');
		if (videoplayerWrapper && this.settings.enabled) {
			videoplayerWrapper.style.left = '';
			videoplayerWrapper.style.right = '0px';
			videoplayerWrapper.style.transition = 'all 500ms ease 0ms';
		} else if (videoplayerWrapper) {
			videoplayerWrapper.style.left = '0px';
			videoplayerWrapper.style.right = '';
		}
	}

	moveChat() {
		setTimeout(() => {
			const screenHasPlayer = document.querySelector('.channel-root--watch') != null;
			const chatTransformContainer = document.querySelector('.channel-root__right-column');
			if (chatTransformContainer && this.settings.enabled && screenHasPlayer) {
				chatTransformContainer.style.transform = 'none';
				chatTransformContainer.style.transition = '';
			} else if (chatTransformContainer) {
				chatTransformContainer.style.transform = 'translateX(-34rem) translateZ(0px)';
				chatTransformContainer.style.transition = '';
				setTimeout(() => {
					chatTransformContainer.style.transition = 'transform 500ms ease 0ms';
				}, 50);
			}

			const peopleToggleButton = document.querySelector('[data-test-selector="chat-viewer-list"]')?.parentElement.parentElement;
			if (peopleToggleButton && this.settings.enabled) {
				peopleToggleButton.style.left = '1rem';
			} else if (peopleToggleButton) {
				peopleToggleButton.style.left = '';
			}

			const chatCollapseIcon = document.querySelector('[data-a-target*="right-column__toggle-collapse-btn"]')?.querySelector('path');
			if (chatCollapseIcon && this.settings.enabled) {
				chatCollapseIcon.style.transform = 'rotate(180deg) translate(-100%, -100%)';
			} else if (chatCollapseIcon) {
				chatCollapseIcon.style.transform = '';
			}

			const chatToggleButton = document.querySelector('[data-a-target*="right-column__toggle-collapse-btn"]')?.parentElement.parentElement;
			if (chatToggleButton && this.settings.enabled && screenHasPlayer) {
				chatToggleButton.style.transition = 'none';
				chatToggleButton.style.transform = 'none';

				if (document.querySelector('[data-a-target="right-column-chat-bar-collapsed"]')) {
					chatToggleButton.style.left = '.5rem';
				} else {
					chatToggleButton.style.left = '30rem';
				}
			} else if (chatToggleButton) {
				chatToggleButton.style.transition = 'none';
				chatToggleButton.style.transform = '';
				chatToggleButton.style.left = '';
			}

			if (chatToggleButton && !screenHasPlayer) {
				chatToggleButton.style.left = '-3.5rem';
			}

			if (!screenHasPlayer) {
				this.docClickRegister();
			} else {
				document.onclick = null;
			}
			
			const chatContainer = document.querySelector('.toggle-visibility__right-column');
			if (chatToggleButton && this.settings.enabled) {
				chatContainer.removeEventListener('click', () => this.showHideChat(this.settings.enabled));
				chatContainer.addEventListener('click', () => this.showHideChat(this.settings.enabled));
			} else if (chatContainer) {
				chatContainer.removeEventListener('click', () => this.showHideChat(this.settings.enabled));
			}
		}, 100);
	}

	moveSidenav() {
		setTimeout(() => {
			const sideNavHeaderTitle = document.querySelector('.side-nav-header');
			if (sideNavHeaderTitle && this.settings.enabled) {
				sideNavHeaderTitle.style.display = 'flex';
				sideNavHeaderTitle.style.justifyContent = 'end';
			} else if (sideNavHeaderTitle) {
				sideNavHeaderTitle.style.display = '';
				sideNavHeaderTitle.style.justifyContent = '';
			}
		}, 50);

		const sideNavToggleButton = document.querySelector('[data-test-selector="side-nav__visibility-toggle"]')?.parentElement;
		if (sideNavToggleButton && this.settings.enabled) {
			sideNavToggleButton.firstChild.removeEventListener('click', () => this.showHideNav(this.settings.enabled));
			sideNavToggleButton.firstChild.addEventListener('click', () => this.showHideNav(this.settings.enabled));
			sideNavToggleButton.style.marginRight = '19rem';
		} else if (sideNavToggleButton) {
			sideNavToggleButton.firstChild.removeEventListener('click', () => this.showHideNav(this.settings.enabled));
			sideNavToggleButton.style.marginRight = '0rem';
		}

		const sideNavToggleIcon = document.querySelector('[data-test-selector="side-nav__visibility-toggle"]')?.querySelector('path');
		if (sideNavToggleIcon && this.settings.enabled) {
			sideNavToggleIcon.style.transform = 'rotate(180deg) translate(-100%, -100%)';
		} else if (sideNavToggleIcon) {
			sideNavToggleIcon.style.transform = '';
		}
	}

	run() {
		if (window.location.pathname == '/') {
			this.docClickRegister();
			this.init();
		} else {
			document.onclick = null;
			this.init();
		}
	}

	cleanup() {
		this.flipFlex();
		this.showHideChat();
		this.moveChannelInfo();
		this.movePlayer();
		this.moveChat();
		this.moveSidenav();
		this.showHideNav();
	}

	always() {
		setTimeout(() => {
			const chatToggleButton = document.querySelector('[data-a-target*="right-column__toggle-collapse-btn"]')?.parentElement.parentElement;
			if (chatToggleButton) {
				chatToggleButton.style.transition = 'none';
			}
		}, 300);
	}
}

export default SwapChat;
