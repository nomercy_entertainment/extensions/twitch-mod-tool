import ChannelPointClicker from './chat/ChannelPointClicker.js';
import SwapChat from './layout/SwapChat.js';

export default [
  new SwapChat(),
  new ChannelPointClicker(),
];
