import Plugin from '../../lib/Plugin.js';

class RouteChangeListener extends Plugin {
	
	constructor() {
		super('Route change listener');
	}

	id = 'cd22e349-77f5-4af2-9486-e9f797f876ff';

	type = 'background';

	settings = {
		enabled: false,
	};
	
	init() {
	}

	run() {
	}

	cleanup() {
	}

	always() {
	}
}

export default RouteChangeListener;
