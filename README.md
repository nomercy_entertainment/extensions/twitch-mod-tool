
# Twitch Tools

* Move the Twitch chat to the left side of the screen.
* Auto channel bonus points redemtion clicker.
* [**upcomming**] Auto raid message sender.
* [**upcomming**] Vertical layout: put the chat under the video for vertical rotated screens.

![image](https://imgur.com/rSf0rN8.png)


## Installation Instructions

### Prerequisites

1. [NodeJS](https://nodejs.org/en/download).
1. [Git](https://git-scm.com/downloads).

### Clone the repository

```bash
git clone https://gitlab.com/nomercy_entertainment/extensions/twitch-mod-tool.git twitch-tools
cd twitch-tools
```

### Install the dependencies

- ##### NPM

```bash
npm install
npm build
```

- ##### Yarn

```bash
yarn
yarn build
```

### Chrome install options

- import the "dist/chrome" folder directly. </br>
- import the "dist/zip/twitch-mod-tool-chrome.zip" directly. </br>
- extract the "dist/zip/twitch-mod-tool-chrome.zip" file and import it. </br>

### Firefox install option

- import the "dist/zip/twitch-mod-tool-firefox.zip" directly. </br>

## Credits

- [Stoney Eagle](https://gitlab.com/Stoney_Eagle)
- [Phil Pelzer](https://gitlab.com/phil.pelzer)

## License

See the [LICENSE](LICENSE) file for license rights and limitations
