# Changelog

* ##### 1.0.1

  * [**build**] Added zipping to the webpack configuration

* ##### 1.0.2

  * [**extension**] Stability fixes.

* ##### 1.1.0

  * [**build**] Added symver updater to webpack configuration.
  * [**extension**] Added channel bonus points redemtion clicker.
  * [**extension**] Added page change event emitter in preparation for upcomming raid message plugin.
  * [**extension**] Stability fixes.
