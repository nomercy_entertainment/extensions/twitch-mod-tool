const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const FileManagerPlugin = require("filemanager-webpack-plugin");
var ZipPlugin = require('zip-webpack-plugin');
const TransformJson = require('transform-json-webpack-plugin');

const entries = ['popup'];

const ignorePatterns = [
  "**/manifest-**",
  "**/dist/**",
  "**/src/**",
  "**/readme.md",
];

module.exports = {
  entry: Object.fromEntries(entries.map(entry => [entry, path.join(__dirname, './extension/', entry, `${entry}.js`)])),
  output: {
    filename: "twitch-mod-tool.js",
    path: path.resolve(__dirname, "dist"),
    clean: true,
  },
  optimization: {
    minimize: false,
  },
  watchOptions: {
    ignored: "**/dist/**",
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: "./extension",
          to: "./chrome",
          globOptions: {
            ignore: ignorePatterns,
          },
        },
        {
          from: "./extension/manifest-chrome.json",
          to: "./chrome/manifest.json",
        },
        {
          from: "./extension",
          to: "./firefox",
          globOptions: {
            ignore: ignorePatterns,
          },
        },
        {
          from: "./extension/manifest-firefox.json",
          to: "./firefox/manifest.json",
        },
      ],
    }),
    new FileManagerPlugin({
      events: {
        onEnd: {
          copy: [
            {
              source: "./extension/dist/**.js",
              destination:
                "./extension/dist/firefox/",
            },
            {
              source: "./extension/dist/**.js",
              destination:
                "./extension/dist/chrome/",
            },
          ],
        },
      },
    }),
    new TransformJson({
      filename: "chrome/manifest.json",
      source: __dirname + "/dist/chrome/manifest.json",
      object: {
        version: process.env.npm_package_version
      }
    }),
    new TransformJson({
      filename:  "firefox/manifest.json",
      source: __dirname + "/dist/firefox/manifest.json",
      object: {
        version: process.env.npm_package_version
      }
    }),
    new TransformJson({
      filename:  "../extension/manifest.json",
      source: __dirname + "/../extension/manifest.json",
      object: {
        version: process.env.npm_package_version
      }
    }),
    new ZipPlugin({
      path: 'zip',
      filename: 'twitch-mod-tool-chrome.zip',
      include: [/chrome\/.*$/],
      pathMapper: function(assetPath) {
        return assetPath.replace(/^\w+[\\\/]/, '');
      },
    }),
    new ZipPlugin({
      path: 'zip',
      filename: 'twitch-mod-tool-firefox.zip',
      include: [/firefox\/.*$/],
      pathMapper: function(assetPath) {
        return assetPath.replace(/^\w+[\\\/]/, '');
      },
    })
  ],
};
